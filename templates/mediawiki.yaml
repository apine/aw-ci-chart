--- # mw-pv-claim
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {{ .Release.Name }}-mw-pv-claim
  labels:
    app: {{ .Release.Name }}-wikifunctions
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 2.5Gi
--- # mediawiki Deployment
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Release.Name }}-mediawiki
spec:
  replicas: 1
  selector:
    matchLabels:
        app: {{ .Release.Name }}-wikifunctions
        tier: mw-fpm
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}-wikifunctions
        tier: mw-fpm
    spec:
      volumes:
      - name: mw-persistent-storage
        persistentVolumeClaim:
          claimName: {{ .Release.Name }}-mw-pv-claim
      containers:
      - name: mw-fpm
        image: docker-registry.wikimedia.org/dev/buster-php74-fpm:1.0.0-s3
        env:
        - name: MW_LOG_DIR
          value: /var/www/html/w/cache
        - name: MW_SCRIPT_PATH
          value: /w
        - name: MW_SERVER
          value: http://wikilambda.home
        - name: XDEBUG_CONFIG
          value: ''
        - name: XDEBUG_ENABLE
          value: "true"
        - name: XHPROF_ENABLE
          value: "true"
        ports:
        - containerPort: 9000
        volumeMounts:
        - name: mw-persistent-storage
          mountPath: /var/www/html 
      initContainers:
      - name: install-mediawiki
        image: docker-registry.wikimedia.org/dev/buster-php74-fpm:1.0.0-s3
        volumeMounts:
        - name: mw-persistent-storage
          mountPath: /var/www/html
        env:
        - name: MW_LOG_DIR
          value: /var/www/html/w/cache
        - name: MW_SCRIPT_PATH
          value: /w
        - name: MW_SERVER
          value: http://wikilambda.home
        - name: XDEBUG_CONFIG
          value: ''
        - name: XDEBUG_ENABLE
          value: "true"
        - name: XHPROF_ENABLE
          value: "true"
        command:
        - bash 
        - -ec
        - |
            mysql --host={{.Values.db.host}} --user={{.Values.db.rootUser}} --password={{.Values.db.rootPassword}} --execute='DROP DATABASE IF EXISTS `wf-{{ .Release.Name }}`'
            cd /var/www/html
            if [[ -f w/initContainer_complete.txt ]]; then
              echo "initContainer has already run successfully"
              exit 0
            elif [ -d w/.git ]; then
              echo "previous run of initContainer failed, sleeping for an hour to give a chance to debug"
              sleep 3600
              exit 1
            fi
            git clone --depth 1 "https://gerrit.wikimedia.org/r/mediawiki/core" w
            git clone --depth 1 "https://gerrit.wikimedia.org/r/mediawiki/skins/Vector" w/skins/Vector
            cp /docker/PlatformSettings.php w/includes
            cd w
            composer update
            php maintenance/install.php \
              --server "http://{{ .Values.mediawikiUrl }}" \
              --scriptpath="/w" \
              --dbtype "mysql" \
              --dbuser "{{ .Values.db.rootUsername }}" \
              --dbpass "{{ .Values.db.rootPassword }}" \
              --dbname "wf-{{ .Release.Name }}" \
              --dbserver "{{ .Values.db.host }}" \
              --lang "en" \
              --pass "{{ .Values.mediawiki.password }}" \
              --with-extensions  \
              "WikiFunctionsTest" "{{ .Values.mediawiki.user }}"
            git clone --depth 1 --recurse-submodules "https://gerrit.wikimedia.org/r/mediawiki/extensions/WikiLambda" extensions/WikiLambda
            if [[ -n "{{ .Values.wikilambdaRef }}" ]]; then
              pushd extensions/WikiLambda
              git fetch https://gerrit.wikimedia.org/r/mediawiki/extensions/WikiLambda {{ .Values.wikilambdaRef}}
              git checkout FETCH_HEAD
              popd
            fi
            git clone --depth 1 "https://gerrit.wikimedia.org/r/mediawiki/extensions/WikimediaMessages" extensions/WikimediaMessages
            echo '{ "extra": { "merge-plugin": { "include": [ "extensions/WikiLambda/composer.json" ] } } }' > composer.local.json
            cat <<- 'NOW' >> LocalSettings.php
            $wgWikiLambdaOrchestratorLocation = 'http://{{ .Release.Name }}-function-orchestrator:6254';
            wfLoadExtension( "WikiLambda" );
            wfLoadExtension( "WikimediaMessages" );
            $wgDefaultSkin = "vector-2022";
            NOW
            php maintenance/run.php createAndPromote --custom-groups functioneer,functionmaintainer --force Admin
            composer update
            php maintenance/update.php --quick
            date > initContainer_complete.txt
--- # mediawiki Service
apiVersion: v1
kind: Service
metadata:
  # NB: mediawiki-web's container image (buster-apache2) assumes that the the
  # mediawiki php service is a "mediawiki"
  # TODO: fixup mediawiki-web to allow a parameterized mediawiki service hostname
  name: mediawiki
  labels:
    app: {{ .Release.Name }}-wikifunctions
    tier: mw-fpm
spec:
  ports:
  - port: 9000
  selector:
    app: {{ .Release.Name }}-wikifunctions
    tier: mw-fpm
--- # mediawiki-web Deployment
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Release.Name }}-mediawiki-web
spec:
  replicas: 1
  selector:
    matchLabels:
        app: {{ .Release.Name }}-wikifunctions
        tier: mw-web
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}-wikifunctions
        tier: mw-web
    spec:
      volumes:
      - name: mw-persistent-storage
        persistentVolumeClaim:
          claimName: {{ .Release.Name }}-mw-pv-claim
      initContainers:
      - name: wait-for-install-to-finish
        image: docker-registry.wikimedia.org/dev/buster:latest
        volumeMounts:
        - name: mw-persistent-storage
          mountPath: /var/www/html
        command:
        - sh
        - -ec
        - |
            # sed -i 's/mediawiki:9000/{{ .Release.Name }}-mediawiki:9000/' /etc/apache2/sites-enabled/000-default.conf
            while [ ! -f /var/www/html/w/initContainer_complete.txt ]; do
              echo "$(date): waiting for /var/www/html/w/initContainer_complete.txt to exist"
              sleep 2
            done

      containers:
      - name: mw-web
        image: docker-registry.wikimedia.org/dev/buster-apache2:2.0.0-s1
        env: 
        - name: MW_LOG_DIR
          value: /var/www/html/w/cache
        ports:
        - containerPort: 8080
        volumeMounts:
        - name: mw-persistent-storage
          mountPath: /var/www/html 
--- # mediawiki-web Service
apiVersion: v1
kind: Service
metadata:
  name: {{ .Release.Name }}-mediawiki-web
  labels:
    app: {{ .Release.Name }}-wikifunctions
    tier: mw-web
spec:
  type: NodePort
  ports:
  - port: 8080
  selector:
    app: {{ .Release.Name }}-wikifunctions
    tier: mw-web
--- # mediawiki-ingress
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: {{ .Release.Name }}-mediawiki-ingress
  #annotations:
  #  nginx.ingress.kubernetes.io/rewrite-target: /$1
spec:
  rules:
    - host: {{ .Values.mediawikiUrl }}
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: {{ .Release.Name }}-mediawiki-web
                port:
                  number: 8080
--- # mediawiki-jobrunner Deployment
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Release.Name }}-mediawiki-jobrunner
spec:
  replicas: 1
  selector:
    matchLabels:
        app: {{ .Release.Name }}-wikifunctions
        tier: mw-jobrunner
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}-wikifunctions
        tier: mw-jobrunner
    spec:
      volumes:
      - name: mw-persistent-storage
        persistentVolumeClaim:
          claimName: {{ .Release.Name }}-mw-pv-claim
      initContainers:
      - name: wait-for-install-to-finish
        image: docker-registry.wikimedia.org/dev/buster:latest
        volumeMounts:
        - name: mw-persistent-storage
          mountPath: /var/www/html
        command:
        - sh
        - -ec
        - |
            while true; do
              if [ -f /var/www/html/w/initContainer_complete.txt ]; then
                exit 0
              fi
              echo "$(date): waiting for /var/www/html/w/initContainer_complete.txt to exist"
              sleep 2
            done
      containers:
      - name: mw-jobrunner
        image: docker-registry.wikimedia.org/dev/buster-php74-jobrunner:1.0.0-s2
        env: 
        - name: MW_LOG_DIR
          value: /var/www/html/w/cache
        - name: MW_INSTALL_PATH
          value: /var/www/html/w
        ports:
        - containerPort: 8080
        volumeMounts:
        - name: mw-persistent-storage
          mountPath: /var/www/html 
