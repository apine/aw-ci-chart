install:
  helm install wikilambda . -f values.yaml

upgrade:
  helm upgrade wikilambda . -f values.yaml

uninstall:
  helm uninstall wikilambda --wait

reinstall: uninstall install

test:
  helm test wikilambda --logs

install-mariadb:
  helm repo add bitnami https://charts.bitnami.com/bitnami
  helm install mariadb bitnami/mariadb --set auth.rootPassword=secret
